from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

import unittest

# Create your tests here.
# Unit Tests
class UnitTest(TestCase):
    def test_url_slash_login_slash_logout_slash_is_exist(self):
        response = Client().get('/login/logout/')
        self.assertEqual(response.status_code, 302)
        
    def test_template_is_used(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_form_is_exist(self):
        response = Client().get('/login/').content.decode('utf-8')
        self.assertIn('form', response)
    
    def test_button_log_in_is_exist(self):
        response = Client().get('/login/').content.decode('utf-8')
        self.assertIn('Login', response)
