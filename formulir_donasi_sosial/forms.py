from django import forms

class FormDonasi(forms.Form):
    # nama = forms.CharField(label="Nama", required=False)
    # email = forms.EmailField(label="Email")
    bank = forms.CharField(label="Nama Bank", required=True)
    norekening = forms.CharField(label="Nomor Rekening", required=True)
    jmldonasi = forms.FloatField(label="Jumlah Donasi", required=True, min_value=5000)
    is_anon = forms.BooleanField(widget=forms.CheckboxInput, required=False, label="Donasi sebagai anonim")

    def clean(self):
        cleaned_data = super().clean()
        # nama = cleaned_data.get("nama")
        is_anon = cleaned_data.get("is_anon")

        

   