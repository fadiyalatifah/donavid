"""donavid URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from django.conf.urls.static import  static
from . import settings
from Login.views import homepage

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls')), 
    path('home/', homepage, name="homepage"),
    path('user/', include("User.urls", namespace="user")),
    path('login/', include(('Login.urls', 'login'), namespace='login')),
    path('testimoni/', include('testimoni.urls')),
    path('donasi-sosial/', include('donasi_sosial.urls')),
    path('formulir_donasi/', include('formulir_donasi_sosial.urls')),
    path('formulir_donasi_apd/', include('formulir_donasi_apd.urls')),
    path('donasi-apd/', include('donasi_apd.urls')),
    path('about-us/', include('about_us.urls')),
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
