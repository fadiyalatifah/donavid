from django.conf.urls import url
from django.shortcuts import render, redirect
from django.urls import reverse

from .forms import (
    RegistrationForm,
)

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required

def register(request):
    if request.method =='POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('Login:login'))
    else:
        form = RegistrationForm()

    args = {'form': form}
    return render(request, 'reg_form.html', args)

@login_required(login_url='/login')
def view_profile(request):
    user = request.user
    args = {
        'user': user,
        
    }
    return render(request, 'profile.html', args)
