from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.forms import ValidationError
from donasi_sosial.models import Program
from .views import formulir, berhasil
from .models import Donasi
from .forms import FormDonasi
import tempfile
import unittest

# Create your tests here.
class Formulir_Donasi_Test(TestCase):
    def test_formulir_donasi_url_is_exist(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        new_program = Program.objects.create(
        name = "Donasi ke korban terdampak pandemi covid",
        photo = image,
        desc ="ayo beri bantuan",
        target = 199000,
        remDay = 20)
        response = Client().get('/formulir_donasi/1')
        self.assertEqual(response.status_code, 200)

    def test_formulir_donasi_using_to_do_list_template(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        new_program = Program.objects.create(
        name = "Donasi ke korban terdampak pandemi covid",
        photo = image,
        desc ="ayo beri bantuan",
        target = 199000,
        remDay = 20)
        response = Client().get('/formulir_donasi/1')
        self.assertTemplateUsed(response, 'formulir_donasi.html')

    def test_formulir_donasi_using_pendaftaran_func(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        new_program = Program.objects.create(
        name = "Donasi ke korban terdampak pandemi covid",
        photo = image,
        desc ="ayo beri bantuan",
        target = 2000000,
        remDay = 24)
        found= resolve('/formulir_donasi/1')
        self.assertEqual(found.func, formulir)

    def test_model_can_create_new_user(self):
        new_user = Donasi.objects.create(nama="me", email="fadiyalatifah@gmail.com",bank="BNI", norekening="087676",jmldonasi=12000, is_anon=False)
        counting_all_user = Donasi.objects.all().count()
        self.assertEqual(counting_all_user, 1)

    def test_model_can_create_anon_user(self):
        new_user = Donasi.objects.create(nama="", email="fadiyalatifah@gmail.com", bank="BNI", norekening="087676", jmldonasi=12000, is_anon=True)
        counting_all_user = Donasi.objects.all().count()
        self.assertEqual(counting_all_user, 1)

    def test_model_user_name_empty_error(self):
        new_user = Donasi.objects.create(nama="", email="fadiyalatifah@gmail.com",bank="BNI", norekening="087676", jmldonasi=12000, is_anon=False)
        self.assertRaises(ValidationError)

    def test_named_UserForm_valid(self):
        form = FormDonasi(data={'nama': "user",'email': "fadiyalatifah@gmail.com" , 'bank':"BNI", 'norekening':"087676",'jmldonasi': "10000", 'is_anon': False})
        self.assertTrue(form.is_valid())

    def test_anon_UserForm_valid(self):
        form = FormDonasi(data={'nama': "",'email': "fadiyalatifah@gmail.com" , 'bank':"BNI", 'norekening':"087676", 'jmldonasi': "10000", 'is_anon': True})
        self.assertTrue(form.is_valid())

    def test_UserForm_invalid(self):
        form = FormDonasi(data={'nama': "",'email': "" , 'bank':"", 'norekening':"", 'jmldonasi': "", 'is_anon': False})
        self.assertFalse(form.is_valid())

class Donasi_Berhasil_Test(TestCase):
    def test_donasi_berhasil_url_is_exist(self):
        response = Client().get('/formulir_donasi/donasi_berhasil/')
        self.assertEqual(response.status_code, 200)

    def test_donasi_berhasil_using_to_do_list_template(self):
        response = Client().get('/formulir_donasi/donasi_berhasil/')
        self.assertTemplateUsed(response, 'donasi_berhasil.html')

    def test_donasi_berhasil_using_pendaftaran_func(self):
        found= resolve('/formulir_donasi/donasi_berhasil/')
        self.assertEqual(found.func, berhasil)