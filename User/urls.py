from django.contrib import admin
from django.urls import path, include

from django.contrib.auth.views import LogoutView
#LoginView,
from django.conf.urls import url
from . import views

app_name = "user"

urlpatterns = [
    path('register', views.register, name="register"),
    path('logout', LogoutView.as_view(), name='logout'),
    path('profile', views.view_profile, name="view_profile")
]
