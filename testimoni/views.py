from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from testimoni.forms import FormTestimoni
from testimoni.models import Testimoni

# Create your views here.
def testimoni(request):
    username = 'anon'
    if request.user.is_authenticated:
        username = request.user
        # print(username)

    if request.POST:
        nama = username
        isi = request.POST.get('isi')
        Testimoni.objects.create(nama=nama, isi=isi)
        return HttpResponseRedirect('/testimoni/')
    else:
        form = FormTestimoni()
        testimonis = Testimoni.objects.all()
        context = {
            'testimonis':testimonis,
            'form':form
        }
        return render(request, 'testimoni.html', context)
    