from django.conf.urls import url
from .views import index, logout_view,login_view

app_name = 'Login'

urlpatterns = [
    url(r'^$', index, name='login'),
    url(r'^validate/$', login_view, name='validate'),
    url(r'^logout/$', logout_view, name='logout')]

