# DONAVID

## Status :
[![pipeline status](https://gitlab.com/fadiyalatifah/donavid/badges/master/pipeline.svg)](https://gitlab.com/fadiyalatifah/donavid/-/commits/master)

[![coverage report](https://gitlab.com/fadiyalatifah/donavid/badges/master/coverage.svg)](https://gitlab.com/fadiyalatifah/donavid/-/commits/master)



## Nama Kelompok :
- Andreas Ilham (1906399543)
- Amanda Carrisa Ashardian (1906399966)
- Bima Sudarsono Adinsa (1906399083)
- Fadiya Latifah (1906399442)
- Faris Muzhaffar (1906400223)

Kelas: PPW-E 

## Link HerokuApp :
https://donavid.herokuapp.com/

## Deskripsi Aplikasi dan Manfaat : 
Pandemi COVID-19 membuat banyak sekali masyarakat Indonesia terkena dampaknya, khususnya dari segi Ekonomi. Oleh karena itu, kami terpikirkan untuk membuat website Donavid yang dapat membantu masyarakat dalam menjalani pandemi ini. Donavid adalah website crowdfunding yang berguna untuk masyarakat yang ingin memberikan donasi kepada orang yang lebih membutuhkan, khususnya kepada petugas medis dan masyarakat yang terkena dampak ekonomi saat pandemi COVID-19. Donavid memiliki 2 fokus tujuan donasi, yaitu donasi khusus pembelian APD untuk petugas medis seperti masker, sarung tangan, baju APD, dan goggles, serta donasi sosial untuk berbagai bantuan yang dibutuhkan masyarakat terdampak COVID-19. Website ini bermanfaat untuk mempermudah orang yang ingin berdonasi kepada masyarakat yang lebih membutuhkan. Website ini juga memungkinkan masyarakat untuk membagikan pesan dan harapan mereka untuk seluruh masyarakat indonesia selama masa pandemi ini


## Daftar fitur dan pembagian kerja :
- Homepage (Andreas Ilham)
- Donasi APD (Amanda Carrisa Ashardian)
- Donasi Sosial (Fadiya Latifah)
- Halaman Testimoni (Faris Muzhaffar)
- Halaman About Us (Faris Muzhaffar)

