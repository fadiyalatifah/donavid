from django.test import TestCase, Client
from .models import UserProfile
from django.contrib.auth.models import User

# Create your tests here.
class PemberianDonasiTest(TestCase):
    def setUp(self):
        self.c = Client()
        self.user = User.objects.create_user(username='amanda',
                                 email='amanda@carrisa.com',
                                 password='amandacarrisa')
        self.userProfile = UserProfile(user=self.user)

    def test_object_to_string(self):
        self.assertEqual(str(self.userProfile), self.userProfile.user.username)

    

    def test_register_url(self):
        response = self.c.get('/user/register')
        self.assertEqual(response.status_code, 200)

    def test_redirect_logout(self):
        response = self.c.get('/user/logout')
        self.assertEqual(response.status_code, 302)

    def test_redirect_blm_login_profile(self):
        response = self.c.get('/user/logout')
        self.assertEqual(response.status_code, 302)

    def test_bikin_user(self):
        response = self.c.post('/user/register',{
           'username': "fadiya",
            'first_name': "fadiya",
            'last_name': "lat",
            'email':"fadiya@lat.com",
            'password1':"fadiyalat",
            'password2': "fadiyalat"
        })
        self.assertEqual(response.status_code, 200) #jadi 200 ya dari 302

    def test_bikin_user_salah(self):
        response = self.c.post('/user/register',{
           'username': "fadiya",
            'first_name': "fadiya",
            'last_name': "lat",
            'email':"fadiya@lat.com",
            'password1':"fadiyalat",
            'password2': "fadiyalat"
        })
        self.assertEqual(response.status_code, 200)

    def test_masuk_fungsi_myprofile(self):
        self.client.login(username='amanda', password='amandacarrisa')
        response = self.client.get('/user/profile')
        self.assertEqual(response.status_code, 200)
