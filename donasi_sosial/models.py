from django.db import models
from formulir_donasi_sosial.models import Donasi

# Create your models here.
class Program(models.Model):
    name   = models.CharField(max_length=200)
    photo  = models.ImageField(upload_to="static/img")
    desc   = models.TextField()
    target = models.IntegerField()
    remDay = models.IntegerField()
    donatur = models.ManyToManyField(Donasi, blank=True)
